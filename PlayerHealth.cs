﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    [Serializable]
    public class PlayerHUD
    {
        
        
        private Slider temperature;
        
        private bool HUDflag = false;

        public bool hudShowing;
        public RaycastHit hitInfo;
        public float percentDecrease, percentIncrease;
        public float timer,deltaTimeCold, deltaTimeWarm;

        public void TemperatureReactToSurfare(GameObject obj)
        {
            if(obj.tag == "ColdArea")
            {
                timer += Time.deltaTime;
                if (timer > deltaTimeCold)
                {
                    temperature.value -= percentDecrease;
                    timer = 0;
                }
            }

            else if(obj.tag == "WarmArea")
            {
                timer += Time.deltaTime;
                if (timer > deltaTimeWarm)
                {
                    temperature.value += percentIncrease;
                    timer = 0;
                }
            }
        }

        public void InitializeHUD()
        {
            temperature = GameObject.FindGameObjectWithTag("TemperatureBar").GetComponent<Slider>();
            temperature.gameObject.SetActive(false);
        }

        public void ShowHUD()
        {
            temperature.gameObject.SetActive(!HUDflag);
            HUDflag = !HUDflag;
        }
    }


    public PlayerHUD playerHUD = new PlayerHUD();

    void Awake()
    {
        playerHUD.percentIncrease = playerHUD.percentDecrease = 1;
        playerHUD.InitializeHUD();
    }
    
    void FixedUpdate()
    {
        
        if(Physics.Raycast(transform.position,transform.TransformDirection(Vector3.down),out playerHUD.hitInfo))
        {
            playerHUD.TemperatureReactToSurfare(playerHUD.hitInfo.transform.gameObject);
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            if (!playerHUD.hudShowing)
            {
                playerHUD.hudShowing = true;
                playerHUD.ShowHUD();
                StartCoroutine("HUDDelay");
            }
        }
    }

    IEnumerator HUDDelay()
    {       
        yield return new WaitForSeconds(5);
        playerHUD.ShowHUD();
        playerHUD.hudShowing = false;
    }

}
